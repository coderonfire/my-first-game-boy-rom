# Build GB rom
main: temp/main.o
	rgblink -o dist/hello-world.gb temp/main.o
	rgbfix -v -p 0 dist/hello-world.gb

temp/main.o: main.asm
	rgbasm -o temp/main.o main.asm
	